import React, {Component} from 'react'
import Value from './Value'
import AdaptabilityImg from './img/CoreValues/Adaptability.png'
import CommunicationImg from './img/CoreValues/Comunication.png'
import CustomerServiceImg from './img/CoreValues/CustomerService.png'
import InitiativeImg from './img/CoreValues/Initiative.png'
import ProfessionalismImg from './img/CoreValues/Professionalism.png'
import TeamWorkImg from './img/CoreValues/TeamWork.png'

const cardStyle = {
    width: '25rem',
};

const divStyle = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '3em',
};

class CoreValue extends Component {

    components = {
        Adaptability: AdaptabilityImg,
        Communication: CommunicationImg,
        CustomerService: CustomerServiceImg,
        Initiative:  InitiativeImg,
        Professionalism: ProfessionalismImg,
        TeamWork: TeamWorkImg
    };

    renderSubcategories() {
        const {subcategories} = this.props.data;
        return (
            subcategories.map((subcategory) =>
                <Value data={{title: subcategory.name}}></Value>
            )
        )
    }

    getCategoryImgPath(category){
        let component = category.replace(" ","");
        return this.components[component]
    }

    render() {
        return (
            <div className="col-md-6" style={divStyle}>
                <div className="card" style={cardStyle}>
                    <img className="card-img-top"
                         src={this.getCategoryImgPath(this.props.data.title)}
                         alt="Card values"/>
                    <div className="card-body">
                        <h5 className="card-title">{this.props.data.title}</h5>
                        <ul className="list-group">
                            {this.renderSubcategories()}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default CoreValue
