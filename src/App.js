import React, {Component} from 'react'
import CoreValue from './CoreValue'
import Thank from './Thank'

class App extends Component {
    state = {
        error: null,
        isLoaded: false,
        categories: [],
        subcategories: [],
    };

    componentDidMount() {
        fetch("http://18.234.234.185:8080/category")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        categories: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );

        fetch("http://18.234.234.185:8080/subcategory")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        subcategories: result
                    });
                },
                (error) => {
                    this.setState({
                        error
                    });
                }
            );
    }

    renderCategories() {
        const {categories, subcategories} = this.state;
        return (
            categories.map((category) =>
                <CoreValue data={{
                    title: category.name,
                    subcategories: this.filterSubcategoriesByCategoryname(category.name, subcategories)
                }}></CoreValue>
            )
        )
    }

    filterSubcategoriesByCategoryname(category, list) {
        let filtered = []
        for (let i = 0; i < list.length; i++) {
            if (list[i].categoryName === category) {
                filtered.push(list[i])
            }
        }
        return filtered
    }

    render() {
        const {error, isLoaded} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="container">
                    <nav className="navbar navbar-light bg-light justify-content-between">
                        <span className="navbar-brand">iThankYou</span>
                        <button type="button" className="btn btn-outline-success my-2 my-sm-0" data-toggle="modal"
                                data-target="#thankModal">Thank
                        </button>
                    </nav>
                    <br/>
                    <div className="row">
                        {this.renderCategories()}
                    </div>
                    <br/>
                    <Thank></Thank>
                </div>
            );
        }
    }
}

export default App
