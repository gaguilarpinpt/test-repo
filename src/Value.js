import React, {Component} from 'react';

class Value extends Component {
    render() {
        return (
            <li className="list-group-item">{this.props.data.title}<span className="badge badge-pill badge-primary">5</span></li>
        );
    }
}

export default Value;