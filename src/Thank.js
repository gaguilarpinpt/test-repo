import React, {Component} from 'react';

class Thank extends Component {
    state = {
        users: []
    }

    componentDidMount() {
    }

    render() {
        //const {users} = this.state
        return (
            <div className="modal fade" id="thankModal" tabIndex={-1} role="dialog" aria-labelledby="thankModalLabel"
                 aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="thankModalLabel">Thank Modal</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form>
                                <div className="form-group">
                                    <label htmlFor="email">Email</label>
                                    <input type="email" className="form-control" id="email" placeholder="Email"/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="main-core-value">Main Core Value</label>
                                    <select id="main-core-value" className="form-control">
                                        <option>""</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="specific-value">Specific Values</label>
                                    <select id="specific-value" className="form-control">
                                    </select>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-success">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Thank;
